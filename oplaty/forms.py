from django import forms
from django.core.exceptions import ValidationError
from django.forms import extras

from crispy_forms.helper import FormHelper

from datetime import date

from .models import (
    Transaction,
    Expense,
    )

class TransactionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TransactionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False

    def clean_amount(self):
        amount = self.cleaned_data["amount"]
        if amount > 0:
            return amount
        else:
            raise ValidationError("Amount must be a positive number.")

    def clean_date(self):
        date = self.cleaned_data["date"]
        if date <= date.today():
            return date
        else:
            raise ValidationError("You can not add transaction in advance.")

    class Meta:
        model = Transaction
        fields = [
            "expense",
            "amount",
            "date",
            "comment",
        ]

        # widgets = {
        #     'date': extras.SelectDateWidget(attrs=({'style': 'width: 20%; display: inline-block;'}))
        # }

class ExpenseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ExpenseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False

    def clean_amount(self):
        amount = self.cleaned_data["amount"]
        if amount > 0:
            return amount
        else:
            raise ValidationError("Amount must be a positive number.")

    class Meta:
        #TODO: HOW TO PASS USER SILENTLY?
        model = Expense
        fields = [
            "title",
            "amount",
            "description",
            "recurring",
            "due_date",
        ]
