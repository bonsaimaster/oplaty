"""bonsai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from .views import (
    home_view,
    month_detail_view,
    year_detail_view,
    expense_create_view,
    expense_detail_view,
    expense_update_view,
    expense_delete_view,
    transaction_create_view,
    transaction_detail_view,
    transaction_update_view,
    transaction_delete_view,
    transaction_list_view,
    )

urlpatterns = [
    url(r'^$', home_view, name='home'),
    url(r'^(?P<year>\d{4})/$', year_detail_view, name='year-view'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$', month_detail_view, name='month-view'),
    url(r'^expense/add/$', expense_create_view, name='expense-create'),
    url(r'^expense/(?P<id>\d+)/$', expense_detail_view, name='expense-detail'),
    url(r'^expense/(?P<id>\d+)/edit/$', expense_update_view, name='expense-update'),
    url(r'^expense/(?P<id>\d+)/delete/$', expense_delete_view, name='expense-delete'),
    url(r'^transaction/add/$', transaction_create_view, name='transaction-create'),
    url(r'^transaction/(?P<id>\d+)/$', transaction_detail_view, name='transaction-detail'),
    url(r'^transaction/(?P<id>\d+)/edit/$', transaction_update_view, name='transaction-update'),
    url(r'^transaction/(?P<id>\d+)/delete/$', transaction_delete_view, name='transaction-delete'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/transactions/$', transaction_list_view, name='transaction-list'),
]
