from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404

from datetime import datetime
import calendar

from .forms import TransactionForm, ExpenseForm
from .models import Expense, Transaction

def transaction_create_view(request):
    form = TransactionForm(request.POST or None)
    context = {
        "form":form,
        "from":request.GET.get("from"),
        }
    if form.is_valid():
        form.save()
        messages.success(request, "Transaction successfully added.")
        previous_page = context["from"]
        if previous_page:
            return redirect(previous_page)
        context = {
            "form":TransactionForm(),
            }
    template = "oplaty/transaction-create-view.html"
    return render(request, template, context)

def transaction_detail_view(request, id=None):
        obj = get_object_or_404(Transaction, id=id)
        context = {
            "transaction":obj,
            "from":request.GET.get("from"),
            }
        template = "oplaty/transaction-detail-view.html"
        return render(request, template, context)

def transaction_update_view(request, id=None):
    obj = get_object_or_404(Transaction, id=id)
    form = TransactionForm(request.POST or None, instance=obj)
    context = {
        "form":form,
        "from":request.GET.get("from"),
        }
    if form.is_valid():
        form.save()
        messages.success(request, "Transaction saved.")
        previous_page = context["from"]
        print(previous_page)
        if previous_page:
            return redirect(previous_page)
        else:
            return redirect("oplaty:transaction-detail", id=id)
    template = "oplaty/transaction-update-view.html"
    return render(request, template, context)

def transaction_delete_view(request, id=None):
        obj = get_object_or_404(Transaction, id=id)
        context = {
        "transaction":obj,
        "from":request.GET.get("from"),
        }
        if request.method == "POST":
            obj.delete()
            messages.success(request, "Transaction successfully deleted.", extra_tags='alert')
            previous_page = context["from"]
            if previous_page:
                return redirect(previous_page)
            else:
                return redirect("oplaty:month-view", year=obj.expense.due_date.year,
                                                     month=obj.expense.due_date.month)
        template = "oplaty/transaction-delete-view.html"
        return render(request, template, context)

def transaction_list_view(request, month=None, year=None):
    transactions = Transaction.objects.by_month(year=year, month=month)
    template = 'oplaty/transaction-list-view.html'
    context = {
        "transactions":transactions,
        "month_name":calendar.month_name[int(month)],
        "month":month,
        "year":year,
        }
    return render(request, template, context)

def expense_create_view(request):
    form = ExpenseForm(request.POST or None)
    context = {
        "form":form,
        "from":request.GET.get("from"),
        }
    if form.is_valid():
        form.save()
        messages.success(request, "Expense successfully added.")
        previous_page = context["from"]
        if previous_page:
            return redirect(previous_page)
        context = {
            "form":ExpenseForm(),
            }
    template = "oplaty/expense-create-view.html"
    return render(request, template, context)

def expense_detail_view(request, id=None):
        obj = get_object_or_404(Expense, id=id)
        transactions = Transaction.objects.filter(expense=obj)
        print(transactions)
        context = {
            "expense":obj,
            "transactions":transactions,
            "from":request.GET.get("from"),
            }
        template = "oplaty/expense-detail-view.html"
        return render(request, template, context)

def expense_update_view(request, id=None):
    obj = get_object_or_404(Expense, id=id)
    form = ExpenseForm(request.POST or None, instance=obj)
    context = {
        "form":form,
        "from":request.GET.get("from")
        }
    if form.is_valid():
        form.save()
        messages.success(request, "Expense saved.")
        return redirect("oplaty:month-view", year=obj.due_date.year,
                                             month=obj.due_date.month)
    template = "oplaty/expense-update-view.html"
    return render(request, template, context)

def expense_delete_view(request, id=None):
        obj = get_object_or_404(Expense, id=id)
        if request.method == "POST":
            obj.delete()
            messages.success(request, "Expense successfully deleted.")
            return redirect("oplaty:month-view", year=obj.due_date.year,
                                                 month=obj.due_date.month)
        context = {
            "expense":obj,
            }
        template = "oplaty/expense-delete-view.html"
        return render(request, template, context)

def home_view(request):
    return redirect("oplaty:year-view", year=datetime.now().year)

def year_detail_view(request, year=None):
    expenses = Expense.objects.year_ordered(year=year)
    recurring = expenses["recurring"]
    non_recurring = expenses["non_recurring"]
    summaries = Expense.objects.year_summary_by_months(year=year)
    months_names = [calendar.month_name[i] for i in range(1, 13)]

    template = 'oplaty/year-view.html'
    context = {
        "summaries":summaries,
        "recurring":recurring,
        "non_recurring":non_recurring,
        "months_names":months_names,
        "year":year,
        }
    return render(request, template, context)


def month_detail_view(request, month=None, year=None):
    exps_month_summary = Expense.objects.month_summary(year=year, month=month)
    expenses = Expense.objects.month_ordered(year=year, month=month)
    transactions = Transaction.objects.by_month_binded(year=year, month=month)
    recurring_expenses = expenses["recurring"]
    non_recurring_expenses = expenses["non_recurring"]
    template = 'oplaty/month-view.html'
    context = {
        "from":request.GET.get("from"),

        "transactions":transactions,

        "recurring_expenses":recurring_expenses,
        "recurring_amount_sum":exps_month_summary["recurring_amount_sum"],
        "recurring_remaining_sum":exps_month_summary["recurring_remaining_sum"],
        "recurring_amount_paid_sum":exps_month_summary["recurring_amount_paid_sum"],

        "non_recurring_expenses":non_recurring_expenses,
        "non_recurring_amount_sum":exps_month_summary["non_recurring_amount_sum"],
        "non_recurring_remaining_sum":exps_month_summary["non_recurring_remaining_sum"],
        "non_recurring_amount_paid_sum":exps_month_summary["non_recurring_amount_paid_sum"],

        "total_amount_sum":exps_month_summary["total_amount_sum"],
        "total_remaining_sum":exps_month_summary["total_remaining_sum"],
        "total_amount_paid_sum":exps_month_summary["total_amount_paid_sum"],

        "month_name":calendar.month_name[int(month)],
        "month":month,
        "year":year,
        }
    return render(request, template, context)
