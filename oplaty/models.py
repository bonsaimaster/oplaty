from django.conf import settings
from django.db import models
from django.db.models import Count, Avg

import calendar
from datetime import datetime, date

class ExpensesManager(models.Manager):
    def month_summary(self, month, year):
        data = {}
        expenses = self.by_month(month=month, year=year)
        recurring = expenses.filter(recurring=True)
        non_recurring = expenses.filter(recurring=False)

        data["recurring_amount_sum"] = sum([e.amount for e in recurring])
        data["recurring_remaining_sum"] = sum([e.remaining_amount for e in recurring])
        data["recurring_amount_paid_sum"] = sum([e.amount_paid for e in recurring])

        data["non_recurring_amount_sum"] = sum([e.amount for e in non_recurring])
        data["non_recurring_remaining_sum"] = sum([e.remaining_amount for e in non_recurring])
        data["non_recurring_amount_paid_sum"] = sum([e.amount_paid for e in non_recurring])

        data["total_amount_sum"] = sum([e.amount for e in expenses])
        data["total_remaining_sum"] = sum([e.remaining_amount for e in expenses])
        data["total_amount_paid_sum"] = sum([e.amount_paid for e in expenses])
        return data

    # def year_by_months(self, year):
    #     months = []
    #     for month in range(1, 13):
    #         months.append(Expense.objects.filter(due_date__month=month, due_date__year=year))
    #     return months

    def year_summary_by_months(self, year):
        return [self.month_summary(month=month, year=year) for month in range(1, 13)]

    def by_month(self, month, year):
        """Returns QS of all expenses fitlered by month and year"""
        return super().get_queryset().filter(due_date__month=month,due_date__year=year)

    def _ordered_titles(self, year, month=None):
        """Split and order expenses' TITLES by recurring and non-recurring,
           * (recurring) ordered by highest average amount,
           * (non-recurring) ordered by most frequent,
           returns: dict {"recurring":[title1, titl2, ...],
                         "non_recurring":[title1, title2, ...]}
        """
        if month:
            expenses = Expense.objects.filter(due_date__month=month, due_date__year=year)
        else:
            expenses = Expense.objects.filter(due_date__year=year)

        non_recurring_order = expenses.filter(recurring=False).values_list('title', flat=True).annotate(title_count=Count('title')).order_by('-title_count')
        recurring_titles = set(expenses.filter(recurring=True).values_list('title', flat=True))
        recurring_avgs_order = {title: expenses.filter(title=title).aggregate(Avg('amount'))['amount__avg'] for title in recurring_titles}
        recurring_avgs_order = sorted(recurring_avgs_order.items(), key=lambda kv: kv[1],  reverse=True)
        recurring_avgs_order = [tup[0] for tup in recurring_avgs_order]

        return {"recurring":recurring_avgs_order, "non_recurring":non_recurring_order}

    def year_ordered(self, year):
        """- list of recurring and non-recurring expenses
           * (recurring) ordered by highest average amount
           * (non-recurring) ordered by most frequent
           * as a list of amounts for each month from Jan
         returns: dict:
            "recurring":[{"title":"expense_title_1","months":[exp1, None..exp2]}, ..],
            (same for non-recurring)
        """
        titles = self._ordered_titles(year=year)

        recurring_ordered = []
        for title in titles["recurring"]:
            data = {"title":title, "months":[]}
            for month in range(1, 13):
                try:
                    data["months"].append(Expense.objects.get(title=title, due_date__month=month, due_date__year=year))
                except Expense.DoesNotExist:
                    data["months"].append(None)
            recurring_ordered.append(data)

        non_recurring_ordered = []
        for title in titles["non_recurring"]:
            data = {"title":title, "months":[]}
            for month in range(1, 13):
                try:
                    data["months"].append(Expense.objects.get(title=title, due_date__month=month, due_date__year=year))
                except Expense.DoesNotExist:
                    data["months"].append(None)
            non_recurring_ordered.append(data)

        return {"recurring":recurring_ordered, "non_recurring":non_recurring_ordered}

    def month_ordered(self, month, year):
        titles = self._ordered_titles(month=month, year=year)
        recurring = [Expense.objects.get(title=title, due_date__month=month, due_date__year=year) for title in titles["recurring"]]
        non_recurring = [Expense.objects.get(title=title, due_date__month=month, due_date__year=year) for title in titles["non_recurring"]]
        return {"recurring":recurring, "non_recurring":non_recurring}

class Expense(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title = models.CharField(max_length=120)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    description = models.TextField(null=False, blank=True)
    recurring = models.BooleanField(default=False)
    due_date = models.DateField(null=False, default=datetime.today)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    objects = ExpensesManager()

    def __str__(self):
        return "{} {}".format(self.due_date, self.title)

    class Meta:
        ordering = ["-due_date"]
        unique_together = ('user', 'title', 'due_date')

    @property
    def amount_paid(self):
        transactions = Transaction.objects.filter(expense=self)
        return sum([t.amount for t in transactions])

    @property
    def remaining_amount(self):
        return self.amount_paid - self.amount


class TransationsManager(models.Manager):
    def by_month(self, month, year):
        return super().get_queryset().filter(date__month=month, date__year=year)

    def by_month_binded(self, month, year):
        expenses = Expense.objects.by_month(month=month, year=year)
        transactions = []
        tmp = [e.transactions.all() for e in expenses]
        [transactions.extend(v) for v in tmp]
        return transactions

    def month_sum_binded(self, month, year):
        transactions = self.by_month_binded(month=month, year=year)
        return sum([t.amount for t in transactions])

    def month_sum(self, month, year):
        transactions = self.by_month(month=month, year=year)
        return sum([t.amount for t in transactions])



class Transaction(models.Model):
    expense = models.ForeignKey(Expense, related_name='transactions', blank=False, null=False)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    comment = models.TextField(null=True, blank=True)
    date = models.DateField(default=datetime.today)

    objects = TransationsManager()

    def __str__(self):
        return "{} / {} ({})".format(self.date, self.amount, self.expense.title)

    #TODO: No idea how to get this to work??
    class Meta:
        ordering = ["-amount"]
